.. groupsolver documentation master file, created by
   sphinx-quickstart on Tue Jan  8 01:10:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to groupsolver's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================


.. toctree::
	apidoc/modules

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
