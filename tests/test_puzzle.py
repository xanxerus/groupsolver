#!/usr/bin/env python3

import pickle

from unittest import TestCase
from groupsolver.shelf import Cubominx, Skewb, SkewbUltimate, RubiksCube
from groupsolver.puzzle import StateSet

class PuzzleTester(TestCase):
    def test_moves(self):
        P = Skewb()
        p = P.instance()
        self.assertEqual(p.states, {
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7]
            }, 'edges': {
                'permutations': [0, 1, 2, 3, 4, 5]
            }
        })

        p.apply('F')
        self.assertEqual(p.states, {
            'corners': {
                'orientations': [2, 1, 0, 1, 1, 0, 0, 0],
                'permutations': [0, 3, 2, 4, 1, 5, 6, 7]
            }, 'edges': {
                'permutations': [2, 0, 1, 3, 4, 5]
            }
        })

        p.apply("R' U' L")
        self.assertEqual(p.states, {
            'corners': {
                'orientations': [2, 0, 1, 2, 0, 1, 1, 2],
                'permutations': [0, 4, 2, 6, 1, 5, 3, 7]
            }, 'edges': {
                'permutations': [3, 4, 5, 2, 1, 0]
            }
        })

        p.apply("L' U R F'")
        self.assertEqual(p.states, P.blankState())

    def test_search(self):
        P = Skewb()
        uperm = {
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7]
            },
            'edges': {
                'permutations': [2, 0, 1, 3, 4, 5]
            },
        }

        soln = P.search(start=uperm)
        p = P.instance()
        p.states = uperm
        p.apply(' '.join(soln))
        self.assertEqual(p.states, P.blankState())

    def test_bfs(self):
        P = Cubominx()
        sune = {
            'corners': {'orientations': [0, 0, 0, 0]},
            'edges': {'permutations': [2, 0, 1, 3, 4, 5]},
        }

        soln = P.search(traversal='bfs', start=sune, goal=P.blankState())
        p = P.instance()
        p.states = sune
        p.apply(' '.join(soln))
        self.assertEqual(p.states, P.blankState())

    def test_astar(self):
        P = Skewb()
        hperm = {
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7]
            },
            'edges': {
                # 'permutations': [5, 1, 4, 3, 2, 1]
                'permutations': [2, 0, 1, 3, 4, 5]
            },
        }

        soln = P.search(traversal='astar', start=hperm, goal=P.blankState())
        p = P.instance()
        p.states = hperm
        p.apply(' '.join(soln))
        self.assertEqual(p.states, P.blankState())

    def test_idastar(self):
        P = SkewbUltimate()
        fourflip = {
            'edges': {
                'orientations': [2, 0, 2, 2, 2, 0],
                'permutations': [0, 1, 2, 3, 4, 5],
            },
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7],
            }
        }
        soln = P.search(start=fourflip, traversal='idastar', verbose=False,
                        initialBound=12)
        p = P.instance()
        p.states = fourflip
        p.apply(' '.join(soln))
        self.assertEqual(p.states, {
            'edges': {
                'orientations': [0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5],
            },
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7],
            }
        })

    def test_goalsearch(self):
        P = Skewb()
        goal = {
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7]
            },
            'edges': {
                'permutations': [0, 1, 3, 5, 4, 2]
            }
        }

        # F L' U L R F' L F'
        start = {
            'corners': {
                'orientations': [1, 0, 2, 2, 2, 2, 2, 2],
                'permutations': [0, 3, 2, 6, 4, 5, 1, 7]
            },
            'edges': {
                'permutations': [3, 0, 4, 5, 2, 1]
            }
        }

        # soln = P.search(start=start, verbose=True)
        soln = P.search(start=start, goal=goal)
        self.assertTrue(soln)
        p = P.instance()
        p.states = start
        p.apply(" ".join(soln))
        self.assertEqual(p.states, goal)

    def test_pickle(self):
        Q = SkewbUltimate()
        S = pickle.dumps(Q)
        P = pickle.loads(S)

        fourflip = {
            'edges': {
                'orientations': [2, 0, 2, 2, 2, 0],
                'permutations': [0, 1, 2, 3, 4, 5],
            },
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7],
            }
        }
        soln = P.search(start=fourflip, traversal='idastar', verbose=False,
                        initialBound=12)
        p = P.instance()
        p.states = fourflip
        p.apply(' '.join(soln))
        self.assertEqual(p.states, {
            'edges': {
                'orientations': [0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5],
            },
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': [0, 1, 2, 3, 4, 5, 6, 7],
            }
        })

    def test_partial_goal(self):
        P = Skewb()
        goal = {
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': '*',
            },
            'edges': {
                'permutations': [[1, 2], [0, 2], [0, 1], [4, 5], [3, 5], [3, 4]]
            }
        }

        # F L' U L R F' L F'
        start = {
            'corners': {
                'orientations': [1, 0, 2, 2, 2, 2, 2, 2],
                'permutations': [0, 3, 2, 6, 4, 5, 1, 7]
            },
            'edges': {
                'permutations': [3, 0, 4, 5, 2, 1]
            }
        }

        soln = P.search(start=start, goal=goal)
        self.assertIsNotNone(soln)
        p = P.instance()
        p.states = start
        p.apply(" ".join(soln))

        #check that the StateSet thinks this is good
        ss = StateSet(P, goal)
        self.assertTrue(p.states in ss)

        #check that it's actually good
        self.assertEqual(p.states['corners']['orientations'], goal['corners']['orientations'])
        self.assertTrue(p.states['edges']['permutations'][0] in {1, 2})
        self.assertTrue(p.states['edges']['permutations'][1] in {0, 2})
        self.assertTrue(p.states['edges']['permutations'][2] in {0, 1})
        self.assertTrue(p.states['edges']['permutations'][3] in {4, 5})
        self.assertTrue(p.states['edges']['permutations'][4] in {3, 5})
        self.assertTrue(p.states['edges']['permutations'][5] in {3, 4})

    def atest_two_phase(self):
        P = RubiksCube()
        p = P.instance()
        p.apply("F U' F D' R2 B R B' D2 R2 U2 R2 F D2 F2 R2 F' D2 R2 L'")
        soln = p.solve()
        self.assertTrue(soln)
        p.apply(soln)
        self.assertEqual(p.states, P.blankState())
