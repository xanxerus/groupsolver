#!/usr/bin/env python3

from unittest import TestCase
from groupsolver.utils import PrioritySet

class PrioritySetTester(TestCase):

    def test_heapsort(self):
        q = PrioritySet()
        arr = [3, 1, 4, 5, 9, 2, 7, 6, 8]
        expected = []

        for i, x in enumerate(arr):
            self.assertEqual(len(q), i)
            q[i] = x
            self.assertTrue(PrioritySetTester.still_heap(q))
            expected.append((i, x))

        self.assertEqual(len(q), len(arr))
        expected.sort(key=lambda e: e[1])

        popped = 0
        while q:
            self.assertEqual(q.peek(), expected[popped])
            self.assertEqual(q.pop(), expected[popped])
            self.assertTrue(PrioritySetTester.still_heap(q))
            popped += 1
            self.assertEqual(len(q), len(arr)-popped)

    def test_setproperty(self):
        q = PrioritySet()
        wrongArr = [1, 9, 5, 4, 2, 7, 6, 8, 3]
        arr = [3, 1, 4, 5, 9, 2, 7, 6, 8]
        expected = []

        for i, x in enumerate(wrongArr):
            self.assertEqual(len(q), i)
            q[i] = x
            self.assertTrue(PrioritySetTester.still_heap(q))

        for i, x in enumerate(arr):
            self.assertEqual(len(q), len(arr))
            q[i] = x
            self.assertTrue(PrioritySetTester.still_heap(q))
            expected.append((i, x))

        self.assertEqual(len(q), len(arr))
        expected.sort(key=lambda e: e[1])

        popped = 0
        while q:
            self.assertEqual(q.peek(), expected[popped])
            self.assertEqual(q.pop(), expected[popped])
            self.assertTrue(PrioritySetTester.still_heap(q))
            popped += 1
            self.assertEqual(len(q), len(arr)-popped)

    @staticmethod
    def still_heap(q, i=0):
        leftIdx = PrioritySet._left_child_index(i)
        rightIdx = PrioritySet._right_child_index(i)
        hasLeftChild = leftIdx < len(q.arr)
        hasRightChild = rightIdx < len(q.arr)

        if hasLeftChild:
            if q.priorityLut[q.arr[leftIdx]] < q.priorityLut[q.arr[i]]:
                return False

        if hasRightChild:
            if q.priorityLut[q.arr[rightIdx]] < q.priorityLut[q.arr[i]]:
                return False

        if hasLeftChild:
            if not PrioritySetTester.still_heap(q, leftIdx):
                return False

        if hasRightChild:
            if not PrioritySetTester.still_heap(q, rightIdx):
                return False

        return True