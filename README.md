# groupsolver

Python package for solving group based puzzles

Puzzles like the Rubik's cube represent a mathematical group structure. The
groups represented by these puzzles can usually be factored into simpler
subgroups. In general these groups are either permutation subgroups (in which
pieces are permutated with each other) or orientation subgroups (in which
pieces can be in one of N different states of orientation).

For example, the Rubik's cube can be represented by four subgroups:

- The orientation of edges
- The orientation of corners
- The permutation of edges
- The permutation of corners

This principle was used by Herbert Kobiemba's two phase algorithm to represent
the states of the Rubik's cube. The groupSolver project is meant to generalize
this idea to other group based puzzles and create an API for solving or
exploring the these puzzles' group structures given a subgroup factorization.

## Installation

This project is maintained in [GitLab](https://gitlab.com/xanxerus/groupsolver)
and also hosted on [PyPI](https://pypi.org/project/groupsolver/) like all Python
packages are supposed to be. Thus, installing from PyPI can be done with:
`pip install groupsolver`

For developing purposes, it's preferable to use a virtualenv. Use
`python3 -m venv /path/to/virtual/environment/` to create a virtualenv, then cd
to where setup.py is, then run either `python3 setup.py install` or
 `pip3 install .` to install this source.


## Testing

Tests in this project are implemented with the unittest framework. There are
several ways to run these tests, but the simplest is to call 
`python3 setup.py test`


## Documentation

Thanks to Sphinx, documentation can be generated from comments by going to the
`doc` directory and running `make html`. The generated documentation will have
its root at `doc/build/index.html`.

The most recent documentation for the master branch can be found on
[GitLab Pages](https://xanxerus.gitlab.io/groupsolver) if it isn't broken.