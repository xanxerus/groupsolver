#!/usr/bin/env python3

import setuptools
import platform

if not platform.python_version().startswith("3."):
    raise ValueError("Must be using Python >=3")

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(name='groupsolver',
    version='1.0',
    description='Python package for solving group based puzzles',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/xanxerus/groupsolver',
    author='Xander Wong',
    author_email='xanxerus@gmail.com',
    license='MIT',
    packages=setuptools.find_packages(),
    zip_safe=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License"
    ],
    test_suite="tests",
    python_requires='>=3',
)
