#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Utilities and data structures. Internal use only!
"""

class PrioritySet:
    '''
    Implements a min-heap where the priority is separate from
    its associated element, and elements are unique
    '''

    def __init__(self):
        self.ancillaryLut = dict()
        self.priorityLut = dict()
        self.idxLut = dict()
        self.arr = []

    def __setitem__(self, element, value):
        if isinstance(value, tuple):
            priority, ancillary = value[0], value[1:]
        else:
            priority, ancillary = value, None

        if ancillary is not None:
            self.ancillaryLut[element] = ancillary

        if element in self.idxLut:
            self._set_priority(element, priority)
        else:
            self._insert(element, priority)

    def __getitem__(self, element):
        if element in self.ancillaryLut:
            return (self.priorityLut[element],) + self.ancillaryLut[element]
        elif element in self.priorityLut:
            return self.priorityLut[element]
        else:
            return None

    def __len__(self):
        return len(self.arr)

    def __bool__(self):
        return bool(self.arr)

    def __contains__(self, item):
        return item in self.priorityLut

    def get(self, item, default):
        if item in self:
            default = self[item]

        if isinstance(default, tuple):
            return (item,) + default
        else:
            return (item, default)

    def peek(self):
        if self.arr:
            element = self.arr[0]
            if element in self.ancillaryLut:
                return (element, self.priorityLut[element]) \
                       + self.ancillaryLut[element]
            else:
                return (element, self.priorityLut[element])
        else:
            return None

    def pop(self):
        if self:
            element, priority = self._extract_min()
            if element in self.ancillaryLut:
                return (element, priority) + self.ancillaryLut[element]
            else:
                return (element, priority)
        else:
            return None

    def _insert(self, element, priority):
        self.priorityLut[element] = priority
        self.arr.append(element)
        self._percolate_up()

    def _extract_min(self):
        if not self.arr:
            return None

        element = self.arr[0]
        priority = self.priorityLut[self.arr[0]]
        del self.idxLut[element]
        del self.priorityLut[element]

        if len(self.arr) > 1:
            self.arr[0] = self.arr[-1]
            self.idxLut[self.arr[0]] = 0
            del self.arr[-1]
            self._percolate_down()
        else:
            del self.arr[0]

        return element, priority

    def _percolate_down(self, idx=0):
        ogElement = self.arr[idx]
        currIdx, currElement = idx, ogElement

        while True:
            lIdx = PrioritySet._left_child_index(currIdx)
            rIdx = PrioritySet._right_child_index(currIdx)
            swapL, swapR = False, False

            if rIdx < len(self.arr):
                lElement, rElement = self.arr[lIdx], self.arr[rIdx]
                if self.priorityLut[lElement] < self.priorityLut[ogElement]:
                    if self.priorityLut[rElement] < self.priorityLut[lElement]:
                        swapR = True
                    else:
                        swapL = True
                elif self.priorityLut[rElement] < self.priorityLut[ogElement]:
                    swapR = True
                else:
                    break
            elif lIdx < len(self.arr):
                lElement = self.arr[lIdx]
                if self.priorityLut[lElement] < self.priorityLut[ogElement]:
                    swapL = True
                else:
                    break
            else:
                break

            if swapL:
                self.arr[currIdx] = lElement
                self.idxLut[lElement] = currIdx
                currIdx, currElement = lIdx, lElement
            elif swapR:
                self.arr[currIdx] = rElement
                self.idxLut[rElement] = currIdx
                currIdx, currElement = rIdx, rElement

        self.arr[currIdx] = ogElement
        self.idxLut[ogElement] = currIdx

    def _percolate_up(self, idx=None):
        if idx is None:
            idx = len(self.arr) - 1

        ogElement = self.arr[idx]
        currIdx, currElement = idx, ogElement

        while True:
            parentIdx = PrioritySet._parent_index(currIdx)
            if parentIdx < 0:
                break

            parentElement = self.arr[parentIdx]
            if self.priorityLut[parentElement] > self.priorityLut[ogElement]:
                self.arr[currIdx] = parentElement
                self.idxLut[parentElement] = currIdx
                currIdx, currElement = parentIdx, parentElement
            else:
                break

        self.arr[currIdx] = ogElement
        self.idxLut[ogElement] = currIdx

    def _set_priority(self, element, priority):
        oldPriority = self.priorityLut.get(element, None)
        if oldPriority > priority:
            self.priorityLut[element] = priority
            self._percolate_up(self.idxLut[element])
        elif oldPriority < priority:
            self.priorityLut[element] = priority
            self._percolate_down(self.idxLut[element])

    @staticmethod
    def _parent_index(idx):
        return ((idx+1) >> 1) - 1

    @staticmethod
    def _left_child_index(idx):
        return ((idx+1) << 1) - 1

    @staticmethod
    def _right_child_index(idx):
        return (idx+1) << 1

    def __str__(self):
        return 'PrioritySet([%s])' % (
            ', '.join('%s:%s' % (e, self.priorityLut[e]) for e in self.arr))
    __repr__ = __str__