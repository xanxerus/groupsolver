#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Mathematical group representation, utilities, and encapsulation

This module provides a means of representing a mathematical group in terms
of a small set of generating elements and their effects on other group elements
under composition.
"""

from copy import deepcopy

class Group:
    """The Group class represents a mathematical group

    A group has a ``size`` N which generally represents the number of pieces of
    a puzzle that this group represents. Those N pieces might be permuted with
    each other, or they might each might be in one of M (some ``modulus``)
    orientations.

    Elements of this group are represented by dicts with at least one of two
    optional key/value pairs, namely the key 'permutations' with a list of
    numbers from 0 to N-1 representing the permutation of the pieces with
    respect to the identity, and the key 'orientations' with a list of numbers
    from 0 to M-1 representing the orientation of the pieces.

    For example, consider a group representing 12 edge pieces of a puzzle, each
    being in 1 of 2 possible states of orientation has a ``size`` of 12 and a
    ``modulus`` of 2. An element of this group could have the edges 1, 4, 7,
    and 8 having orientation 1 and the rest have orientation 0, and also edge 1
    is in position 4, 4 is in position 7, 7 in position 8, and 8 in 1. Then
    the group element would look like::

        {
            'permutations': [0, 8, 2, 3, 1, 5, 6, 4, 7, 9, 10, 11],
            'orientations': [0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0]
        }

    The group itself is defined by its generating elements, which can be
    derived from and interpreted as the different moves of the puzzle. Moves
    are defined by their affect on the ``permutations`` of an element and also
    by their affect on the ``orientations`` of an element. These effects are
    called actions. Orienting actions will occur before permuting actions for
    ease of notation. The ``permutations`` and ``orientations`` arguments map
    move names of type str to their corresponding actions.

    An orientation action is defined by a dict mapping index to delta,
    where the index is the index of the piece to be reoriented
    and the delta is the amount by which the piece should be reoriented.
    Negative deltas are allowed. Every piece in the group must have the same
    modulus. For example, an orientation action that flips edges 1, 4, 7, and 8
    might look like::

        {
            1: 1,
            4: 1,
            7: 1,
            8: 1
        }

    A permutation action is defined in cycle notation with 'is replaced by'
    ordering and indexed at 0. So the action [0, 2, 1] would change the state
    [0, 1, 2, 3, 4] into the state [2, 0, 1, 3, 4]. This is the style of
    permutation cycle used in blind solving, but missing the repeated number at
    the end. Multiple cycles can be specified by putting the cycles into a
    list. For example, if a move switches 1 and 2, and cycles 4, 5, and 6::

        [
            [1, 2],
            [4, 5, 6]
        ]

    Often, moves can be inverted, doubled, double and inverted, tripled, and
    so on. To specify these exponents, a list of ``accidentals`` can be
    specified as suffixes to the moves to be interpreted as exponents. For
    example, a group constructed with ``moves=['F', 'U', 'R', 'L']`` and
    ``accidentals=["'", "2", "3'"]`` would have 16 moves, namely F,  U,  R,  L,
    F',  U',  R',  L',  F2,  U2,  R2,  L2,  F3',  U3',  R3',  and L3'.

    Sometimes, it is necessary to only apply accidentals to some moves but not
    others. In this case, a dict can be used, where keys are move names and
    the values are the lists of accidentals. For example::

        {
            'U': ["'", "2"],
            'D': ["'", "2"]
        }

    :param size: int. Number of pieces tracked.
        (Required)
    :param permutations: dict. Maps move strings to permutation actions.
        (Optional unless ``orientations`` is not defined, default None)
    :param orientations: dict. Maps move strings to orientation actions.
        (Optional unless ``permutations`` is not defined, default None)
    :param accidentals: list or dict. Accidentals to use.
        (Optional, default None)
    :param modulus: int. Number of orientation states.
        (Optional unless ``orientations`` is defined, default None)

    """

    # Init functions

    def __init__(self, size=None, permutations=None, orientations=None,
                 accidentals=None, modulus=None, **kwargs):

        basemoves = (permutations.keys() if permutations else set() |
              orientations.keys() if orientations else set())

        accidentals = Group._preprocessAccidentals(basemoves, accidentals)

        # set up actions
        if permutations:
            self.permutations = Group._preprocessPermutations(
                permutations, accidentals)
        else:
            self.permutations = dict()

        if orientations:
            if modulus is None:
                raise ValueError(
                    'A Group with orienting actions must have a modulus')
            self.modulus = modulus
            self.orientations = Group._preprocessOrientations(
                orientations, self.permutations, accidentals, modulus)
        else:
            self.modulus = 1
            self.orientations = dict()

        # find the max length
        if self.permutations:
            maxPerm = max(map(max, map(max, self.permutations.values())))
        else:
            maxPerm = 0

        if self.orientations:
            maxOr = max(max(a.values()) for a in self.orientations.values())
        else:
            maxOr = 0

        maxPos = max(maxPerm, maxOr)

        if size is None:
            self.size = maxPos
        elif maxPos < size:
            self.size = int(size)
        else:
            raise ValueError(
                "Group.size must exceed every position referenced by every "
                "move's action, yet a %d was found when size was %d"
                % (maxPos, size))

        self.moveset = set(self.permutations.keys() | self.orientations.keys())

        # generate the blank state
        self._BLANK_STATE = dict()
        if self.orientations:
            self._BLANK_STATE['orientations'] = [0]*self.size
        if self.permutations:
            self._BLANK_STATE['permutations'] = list(range(self.size))

    # Private helper functions

    @staticmethod
    def _preprocessAccidentals(basemoves, accidentals):
        """
        Given basemoves and accidentals, return a dict which maps moves to the
        list of accidentals that apply to that move.

        :param basemoves: list. A list of move strings.
        :param accidentals: list or dict. A list or dict of accidental strings
            as given to ``__init__``.
        :return: dict. Maps moves to their applicable accidentals.
        """
        ret = dict()
        if accidentals is None:
            for move in basemoves:
                ret[move] = []
        elif isinstance(accidentals, list):
            for move in basemoves:
                ret[move] = list(accidentals)
        elif isinstance(accidentals, dict):
            ret = dict(accidentals)
            for move in basemoves:
                if move not in ret:
                    ret[move] = []
        else:
            raise ValueError("Cannot create accidental dict out of %s"
                             % type(accidentals))
        return ret

    @staticmethod
    def _preprocessPermutations(permutations, accidentals):
        """
        Apply accidentals to each permutation action.

        :param permutations: dict. Maps move strings to permutation actions.
        :param accidentals: dict. Maps move strings to their accidentals.
        :return: dict. A new map from move string to permutation actions which
            has all accidentals applied.
        """
        newMoveset = dict()
        for move, action in permutations.items():
            if not isinstance(action, list) and not isinstance(action, tuple):
                raise ValueError(
                    "PermutationGroup cannot interpret a non-list action")
            elif len(action) and isinstance(action[0], int):
                action = [action]

            newMoveset[move] = action
            for a in accidentals[move]:
                shamt = Group._parseAccidental(a)
                newAction = Group._shiftPermutationAction(action, shamt)
                if newAction :
                    newMoveset[move + a] = newAction

        return newMoveset

    @staticmethod
    def _shiftPermutationAction(action, shamt):
        """
        Shift a permutation action by a given amount.

        :param action: list. A list of cycles to shift.
        :param shamt: int. An amount to shift by. Can be negative or zero.
        :return: list. A list of cycles that have been shifted.
        """
        ret = []
        for cycle in action:
            ret.extend(Group._shiftCycle(cycle, shamt))
        return ret

    @staticmethod
    def _shiftCycle(cycle, shamt=1):
        """
        Shift a cycle by a given amount.

        :param cycle: list. A list of ints in 'is replaced by' order.
        :param shamt: int. An amount to shift by. Can be negative or zero.
        :return: list. A cycle that has been shifted.
        """
        if shamt == 0:
            return []

        ret = [] # copy the first element
        seen = set()
        while len(seen) < len(cycle):
            # find a starting point for a new cycle
            i = 0
            while cycle[i] in seen:
                i += 1
                if i == len(cycle):
                    raise ValueError('Cannot shift cycle: %s by %d'
                                     % (cycle, shamt))
            newCycle = [cycle[i]]

            # get the cycle
            while True:
                i = (i + shamt) % len(cycle)
                e = cycle[i]
                if e != newCycle[0]:
                    newCycle.append(e)
                else:
                    break

            # update the lists and move on
            seen.update(newCycle)
            if len(newCycle) > 1:
                ret.append(newCycle)

        return ret

    @staticmethod
    def _preprocessOrientations(orientations, permutations, accidentals,
                                modulus):
        """
        Apply accidentals to each orientation action.

        :param orientations: dict. Maps move strings to orientation actions.
        :param permutations: dict. Maps move strings to orientation actions.
        :param accidentals: dict. Maps move strings to lists of accidentals.
        :param modulus: int. A modulus to apply after addition.
        :return: dict. A new map from move string to orientation actions which
            has all accidentals applied.
        """
        newMoveset = dict()
        for move, orientingAction in orientations.items():
            if not isinstance(orientingAction, dict):
                raise ValueError(
                    "OrientationGroup cannot interpret a non-dict action")

            newMoveset[move] = orientingAction
            permutingAction = permutations.get(move, tuple())
            for a in accidentals[move]:
                shamt = Group._parseAccidental(a)
                newMoveset[move + a] =  Group._shiftOrientationAction(
                    orientingAction, permutingAction, shamt, modulus)

        return newMoveset

    @staticmethod
    def _shiftOrientationAction(orientingAction, permutingAction, shamt,
                                modulus):
        """
        Multiply an orientation action by a given amount. Apply the permutation
        by that number of times too.

        :param orientingAction: list. A list of amounts to add to each
            element of the 'orientations' list.
        :param permutingAction: list. A list of cycles to apply.
        :param shamt: int. The amount to shift by.
        :param modulus: int. A modulus to apply after addition.
        :return: list. An orienting action that has been shifted.
        """
        ret = dict()
        if shamt < 0:
            for cycle in permutingAction:
                for i in range(len(cycle)):
                    s = 0
                    for j in range(abs(shamt)):
                        s -= orientingAction.get(cycle[(i + j + 1)
                                                       % len(cycle)],
                                                 0)
                    ret[cycle[i]] = s % modulus
        elif shamt > 0:
            for cycle in permutingAction:
                for i in range(len(cycle)):
                    s = 0
                    for j in range(abs(shamt)):
                        s -= orientingAction.get(cycle[(i - j)
                                                       % len(cycle)],
                                                 0)
                    ret[cycle[i]] = s % modulus

        for k in orientingAction:
            if k not in ret:
                delta = (orientingAction[k] * shamt) % modulus
                if delta:
                    ret[k] = delta

        return ret

    @staticmethod
    def _parseAccidental(accidental):
        """
        Given an accidental, calculate its shift amount.

        :param accidental: str. An accidental string.
        :return:
        """
        if accidental == "'":
            return -1

        if accidental.endswith("'"):
            mul = -1
            accidental = accidental[:-1]
        else:
            mul = 1

        try:
            return int(accidental) * mul
        except Exception as e:
            raise ValueError("Cannot convert accidental to integer: %s"
                             % accidental)

    def _applyPermutation(self, move, state):
        """
        Apply a move to a permutation state.

        :param move: str. A move string.
        :param state: list. A list of ints: a permutation.
        :return: list. The new state after applying the move.
        """
        action = self.permutations.get(move, None)
        if action is None:
            return state

        state = list(state)
        for cycle in action:
            temp = state[cycle[0]]
            for i in range(len(cycle)-1):
                state[cycle[i]] = state[cycle[i+1]]
            state[cycle[-1]] = temp
        return state

    def _applyOrientation(self, move, state):
        """
        Apply a move to an orientation state.

        :param move: str. A move string.
        :param state: list. A list of ints: an orientation state.
        :return: list. The new state after applying the move.
        """
        action = self.orientations.get(move, None)
        if action is None:
            return state

        state = list(state)
        for index, delta in action.items():
            state[index] += delta
            state[index] %= self.modulus
        return state

    # Instance methods

    def blankState(self):
        """
        Returns a state representing the Identity element of this group, i.e.,
        a blank state of this group.

        :return: dict. A blank state.
        """
        return deepcopy(self._BLANK_STATE)

    def apply(self, move, element):
        """
        Apply a move to an element of this group. If no such move, do nothing.

        :param move: str. A string representing a move (may contain an
            accidental). (Required)
        :param element: dict. A group element. (Required)
        :return: dict. The group element obtained by applying the move to the
            given element.
        """
        ret = dict()
        if self.orientations:
            ret['orientations'] = self._applyOrientation(
                move, element['orientations'])

        if self.permutations:
            ret['permutations'] = self._applyPermutation(
                move, element['permutations'])
            if self.orientations:
                ret['orientations'] = self._applyPermutation(
                    move, ret['orientations'])
        return ret

    def composeLeftInverse(self, g, h):
        """
        Given two group elements `g` and `h`, return `inverse(g) * h`.

        :param g: dict. A group element.
        :param h: dict. Another group element.
        :return: dict. `inverse(g) * h`
        """

        ret = dict()
        if self.permutations:
            if self.orientations:
                gInvPermutations = [0] * self.size
                gInvOrientations = [0] * self.size

                # invert g
                for toIdx, fromIdx in enumerate(g['permutations']):
                    gInvPermutations[fromIdx] = toIdx
                    gInvOrientations[fromIdx] = (-g['orientations'][toIdx]
                                                 % self.modulus)

                retPermutations = [0] * self.size
                retOrientations = [0] * self.size

                # apply h
                for toIdx, fromIdx in enumerate(h['permutations']):
                    retPermutations[toIdx] = gInvPermutations[fromIdx]
                    retOrientations[toIdx] = ((h['orientations'][toIdx]
                                               + gInvOrientations[fromIdx])
                                              % self.modulus)

                ret['permutations'] = retPermutations
                ret['orientations'] = retOrientations
            else:
                gInvPermutations = [0] * self.size

                # invert g
                for toIdx, fromIdx in enumerate(g['permutations']):
                    gInvPermutations[fromIdx] = toIdx

                retPermutations = [0] * self.size

                # apply h
                for toIdx, fromIdx in enumerate(h['permutations']):
                    retPermutations[toIdx] = gInvPermutations[fromIdx]

                ret['permutations'] = retPermutations

        elif self.orientations:
            retOrientations = [0] * self.size
            for i, e in enumerate(g['orientations']):
                retOrientations[i] = (h['orientations'][i] - e) % self.modulus
            ret['orientations'] = retOrientations

        return ret

    # Override functions

    def __str__(self):
        ret = ['Group']
        for k in sorted(self.moveset):
            p = self.permutations.get(k, '<>')
            o = self.orientations.get(k, '<>')
            ret.append("  %-4s:  %s  --  %s" % (k, p, o))
        return '\n'.join(ret)

    __repr__ = __str__

    def __eq__(self, other):
        return all((
            isinstance(other, Group),
            self.permutations == other.permutations,
            self.orientations == other.orientations,
            self.size == other.size,
            self.modulus == other.modulus
        ))

    def __ne__(self, other):
        return not (self == other)