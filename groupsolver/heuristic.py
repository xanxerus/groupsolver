#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generation and querying of heuristics based on pruning tables
"""

from collections import deque

class Heuristic:
    """
    Turns a list of pruning table descriptions into a list of
    :class:`groupsolver.heuristic.PruningTable` objects and stores a them
    to be queried later as a means of providing lower bounds for a given
    :class:`groupsolver.puzzle.Puzzle` state's distance from the solved state
    based on those pruning tables.

    :param puzzle: Puzzle. The puzzle that this heuristic is relevant to.
    :param tables: list. A list of Pruning table descriptions
    :param moveset: list. A list of move strings. (Optional, default None
            will take this moveset from the Puzzle.moveset)
    :param strict: bool. Whether or not to raise an exception if a Puzzle
        states dict is queried but not found in any pruning table. (Optional,
        default False)
    """

    def __init__(self, puzzle, tables=None, moveset=None, strict=False,
                 **kwargs):

        self.strict = strict

        if moveset is None:
            self.moveset = set(puzzle.moveset)
        elif hasattr(moveset, '__iter__'):
            moveset = set(moveset)
            if not all(isinstance(e, str) for e in moveset):
                raise ValueError(
                    "Expected moveset to be iterable containing str types. "
                    "See: %s" % moveset)
            self.moveset = moveset
        else:
            raise ValueError(
                "Expected moveset to be iterable. See: %s" % moveset)


        if not hasattr(tables, '__iter__'):
            raise ValueError(
                "Expected tables to be iterable. See: %s" % tables)
        else:
            #validate each tableSpec
            finalTableSpecs = []
            for tableSpec in tables:
                if not hasattr(tableSpec, '__iter__'):
                    raise ValueError(
                        "Expected all members of 'tables' to be dict. "
                        "See: %s" % tableSpec)

                #get maximumDepth
                myTableSpec = dict()
                if 'maximumDepth' in tableSpec:
                    try:
                        myTableSpec['maximumDepth'] = \
                            int(tableSpec['maximumDepth'])
                    except ValueError:
                        raise ValueError(
                            "Could not convert maximum depth to int. "
                            "See: %s" % tableSpec['maximumDepth'])
                else:
                    myTableSpec['maximumDepth'] = 0

                if 'groups' not in tableSpec:
                    raise ValueError(
                        "Cannot have a tableSpec without groups. "
                        "See: %s" % tableSpec)

                #validate each groupSpec
                myGroupList = []
                for groupSpec in tableSpec['groups']:
                    myGroupSpec = dict()

                    if not isinstance(groupSpec, dict):
                        raise ValueError(
                            "Expected all group specifers to be dict. "
                            "See: %s" % groupSpec)

                    if 'group' in groupSpec:
                        if isinstance(groupSpec['group'], str):
                            myGroupSpec['group'] = groupSpec['group']
                        else:
                            raise ValueError(
                                "Expected group specifier's 'group' value to "
                                "be str. See: %s" % groupSpec)
                    else:
                        raise ValueError(
                            "Expected group specifier to have a 'group' "
                            "value. See: %s" % groupSpec)

                    if 'subgroup' in groupSpec:
                        if isinstance(groupSpec['subgroup'], str):
                            myGroupSpec['subgroup'] = groupSpec['subgroup']
                        else:
                            raise ValueError(
                                "Expected group specifier's 'subgroup' value "
                                "to be str. See: %s" % groupSpec)
                    else:
                        myGroupSpec['subgroup'] = 'all'

                    if 'track' in groupSpec:
                        if not hasattr(groupSpec['track'], '__iter__'):
                            raise ValueError(
                                "Expected group specifier's 'track' value to "
                                "be iterable. See: %s" % groupSpec)
                        else:
                            track = list(groupSpec['track'])
                            if not all(isinstance(e, int) for e in track):
                                raise ValueError(
                                    "Expected group specifier's 'track' value "
                                    "to contain only ints. See: %s"
                                    % groupSpec)

                            myGroupSpec['track'] = track
                            if 'distinctive' in groupSpec:
                                if isinstance(groupSpec['distinctive'], bool):
                                    myGroupSpec['distinctive'] = \
                                        groupSpec['distinctive']
                                else:
                                    raise ValueError(
                                        "Expected group specifier's "
                                        "'distintive' value to be bool. "
                                        "See: %s" % groupSpec)

                    myGroupList.append(myGroupSpec)

                myTableSpec['groups'] = myGroupList
                finalTableSpecs.append(myTableSpec)

        #create tables
        self.tables = []
        for tableSpec in finalTableSpecs:
            self.tables.append(PruningTable(
                puzzle, moveset=self.moveset, **tableSpec))

    def __call__(self, states):
        """
        Given a Puzzle states dict, return a lower bound for the number of
        moves required to solve the state based on the pruning tables generated
        in the constructor.

        :param states: dict. A puzzle states dict.
        :return: int. A lower bound on the number of moves needed to solve
            the puzzle state.
        """
        ret = 0
        for table in self.tables:
            pot = table.get(states, None)
            if pot is not None:
                ret = max(ret, pot)
            elif self.strict:
                raise ValueError(
                    "Pruning Table for %s failed to classify state %s, and "
                    "strict mode is on." % (table.groupSpecs, states))
        return ret

    def __contains__(self, state):
        """
        Determine if the given Puzzle states dict has a heuristic distance of
        0.

        :param state: A puzzle states dict.
        :return: True if the states dict is solved, False otherwise.
        """
        return self(state) == 0

    def __str__(self):
        ret = ['Heuristic:']
        for t in self.tables:
            ret.append("  %s" % (t))
        return '\n'.join(ret)

    __repr__ = __str__

    @staticmethod
    def simple(puzzle):
        """
        Return a Heuristic with one Pruning table for each subgroup of a given
        Puzzle.

        :param puzzle: Puzzle. The puzzle to create a Heuristic for.
        :return: A Heuristic as mentioned above.
        """
        tables = []
        for groupName, group in puzzle.groups.items():
            if group.orientations:
                tables.append({
                    'groups': [{
                        'group': groupName,
                        'subgroup': 'orientations'
                    }],
                    'maximumDepth': 0
                })
            if group.permutations:
                tables.append({
                    'groups': [{
                        'group': groupName,
                        'subgroup': 'permutations'
                    }],
                    'maximumDepth': 0
                })

        return Heuristic(puzzle=puzzle, tables=tables)

class PruningTable:
    """
    Generates and stores a pruning table based on a given moveset,
    distinguishing only the specifically given parts of group element states,
    tracking only those states which are within a given maximumDepth number
    of moves from the solved state.

    The parts of groups that are relevant to the PruningTable are specified
    by dicts with the following format ::

        {
            'group': <str. A group name>,
            'subgroup': <str. A subgroup name. Optional. If omitted, then
                both permutations and orientations are used.>,
            'track': <list of int. Indices less than group.size. Optional.
                If omitted, then all indices are tracked.>,
            'distinctive': <bool. Whether or not tracked indices are distinct.
                Optional. If omitted, then all indices are distinctive.>
        }

    For example, to generate the pruning table for the UD-Slice in Phase 1
    of Kociemba's two phase algorithm for the Rubik's cube, one could write ::

        {
            'group': 'edges',
            'subgroup': 'permutations',
            'track': [4, 5, 6, 7],
            'distinctive': False
        }

    This group specification means that only the positions (and not the
    orientations) of edges 4, 5, 6, and 7 are tracked, and there is no
    distinction between, for example, permutation state
    ``[0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12]`` or
    ``[0, 1, 2, 3, 4, 6, 5, 7, 8, 10, 11, 12]`` or
    ``[0, 1, 2, 3, 5, 7, 6, 4, 8, 10, 11, 12]`` or
    ``[0, 1, 2, 3, 7, 6, 5, 4, 8, 10, 11, 12]`` and so on.

    :param puzzle: Puzzle. A puzzle that this PruningTable is relevant to.
    :param groups: list. A list of dicts describing parts of groups to take
        note of while generating the PruningTable. See above.
    :param moveset: list. A list of move strings. (Optional, default None
        will take this moveset from the Puzzle.moveset)
    :param maximumDepth: int. A maximum depth of the search tree. (Default 0
        means there is no maximum depth)
    """

    def __init__(self, puzzle, groups=None, moveset=None, maximumDepth=0):

        self.groupSpecs = PruningTable._validateGroups(puzzle, groups)

        if moveset is None:
            moveset = set(puzzle.moveset)

        if not isinstance(maximumDepth, int):
            raise ValueError("Expected maximumDepth to be of type int. "
                             "See: %s" % maximumDepth)
        else:
            self.maximumDepth = maximumDepth

        #generate the reverse table
        reverseTable = []
        q = deque()
        q.append([puzzle.blankState(), 0, None])
        while q:
            state, depth, lastMove = q.popleft()
            fState = self._flatten(state)

            #if we've seen this before, we're not getting a better result
            for tStates in reverseTable:
                if fState in tStates:
                    break

            #we enter this 'else' clause if we've never seen this state before
            else:
                #update the table's entry for this state
                if depth < len(reverseTable):
                    reverseTable[depth].add(fState)
                else:
                    # print (list(map(len, reverseTable)))
                    reverseTable.append({fState})

                if self.maximumDepth > 0 and depth >= self.maximumDepth:
                    continue

                #add the next states to the queue
                for move in moveset:
                    if lastMove is None or lastMove[0] != move[0]:
                        nextState = puzzle.apply_move(state, move)
                        q.append([nextState, depth+1, move])

        # print(list(map(len, reverseTable)))
        self.reverseTable = reverseTable

    @staticmethod
    def _validateGroups(puzzle, groups):
        """
        Validates a group specification list with respect to a given puzzle.

        :param puzzle: Puzzle. A puzzle.
        :param groups: list. A list of dicts of group specifications.
        :return: list. A list of dicts of group specifications that is
            guaranteed to be valid.
        """

        if groups is None:
            raise ValueError("Expected groups to be an __iter__. "
                             "See: %s" % groups)

        if not hasattr(groups, '__iter__'):
            raise ValueError("Expected 'groups' to be iterable. "
                             "See: %s" % groups)

        myGroupSpecs = []
        for groupSpec in groups:
            if not isinstance(groupSpec, dict):
                raise ValueError("Expected all group specifications to"
                                 "be of type dict. See %s" % groupSpec)

            myGroupSpec = dict()
            if 'group' in groupSpec:
                if not groupSpec['group'] in puzzle.groups:
                    raise ValueError("No group named %s in given puzzle. "
                                     % groupSpec['group'])

                myGroupSpec['group'] = groupSpec['group']
            else:
                raise ValueError(
                    "Group specification must have a group name. See: %s"
                    % groupSpec)

            if 'subgroup' in groupSpec:
                if (not puzzle.groups[groupSpec['group']].orientations
                        and groupSpec['subgroup'] == 'orientations'):
                    raise ValueError(
                        "Group named %s has no orientation subgroup"
                        % groupSpec['group'])
                elif (not puzzle.groups[groupSpec['group']].orientations
                        and groupSpec['subgroup'] == 'orientations'):
                    raise ValueError(
                        "Group named %s has no orientation subgroup"
                        % groupSpec['group'])
                elif (groupSpec['subgroup']
                        not in ('orientations', 'permutations')):
                    raise ValueError(
                        "%s is not a valid subgroup"
                        % puzzle.groups['subgroup'])

                myGroupSpec['subgroup'] = groupSpec['subgroup']
            else:
                myGroupSpec['subgroup'] = 'all'

            if 'track' in groupSpec:
                if not hasattr(groupSpec['track'], '__iter__'):
                    raise ValueError("Expected group specification's "
                                     "'track' value to be iterable. "
                                     "See: %s" % groupSpec['track'])

                myTrack = list(groupSpec['track'])

                if max(myTrack) >= puzzle.groups[groupSpec['group']].size:
                    raise ValueError(
                        "Indices cannot exceed the group size. See: %s"
                        % myTrack)
                elif min(myTrack) < 0:
                    raise ValueError(
                        "Indices cannot be less than 0. See: %s"
                        % myTrack)
                elif not all(isinstance(e, int) for e in myTrack):
                    raise ValueError(
                        "Expected all track indices to be of type int. "
                        "See: %s" % myTrack)

                myGroupSpec['track'] = myTrack

                if 'distinctive' in groupSpec:
                    if not isinstance(groupSpec['distinctive'], bool):
                        raise ValueError("Expected group specification's "
                                         "'distinctive' value to be of "
                                         "type bool. See: %s"
                                         % groupSpec['distinctive'])

                    myGroupSpec['distinctive'] = groupSpec['distinctive']

            myGroupSpecs.append(myGroupSpec)

        return myGroupSpecs

    def _flatten(self, states):
        """
        Turns a Puzzle states dict into a tuple of ints in a predictable order,
        namely in order of Group first, and putting orientations before
        permutations, concatenating all states, and including only those
        indices which are necessary to maintain distinctiveness and nothing
        more.

        :param states: dict. A Puzzle states dict.
        :return:
        """
        ret = []
        for groupSpec in self.groupSpecs:
            groupName, subgroupName = groupSpec['group'], groupSpec['subgroup']
            track = groupSpec.get('track', None)
            distinctive = groupSpec.get('distinctive', None)
            if subgroupName == 'all':
                if 'orientations' in states[groupName]:
                    ret.extend(self._flatten_track(
                        states[groupName]['orientations'],
                        track,
                        distinctive
                    ))
                if 'permutations' in states[groupName]:
                    ret.extend(self._flatten_track(
                        states[groupName]['permutations'],
                        track,
                        distinctive
                    ))
            else:
                ret.extend(self._flatten_track(
                    states[groupName][subgroupName],
                    track,
                    distinctive
                ))
        return tuple(ret)

    def _flatten_track(self, state, track=None, distinctive=False):
        """
        Turns a group element's state list into a possibly shorter list of
        ints, namely only those which are being tracked.

        If ``distinctive`` is false, then only the positions of those pieces
        are kept, not which piece was which.

        :param state: list. A list of ints taken from a group element.
        :param track: list. A list of ints specifying indices of ints within
            the state which need to be tracked..
        :param distinctive: bool. Whether or not the positions of a given
            state's tracked indices are distinctive between elements with
            respect to this PruningTable.
        :return:
        """

        if track is None:
            return state

        if distinctive:
            ret = []
            for t in enumerate(track):
                ret.append(state.index(t))
            return ret
        else:
            ret = []
            for i, s in enumerate(state):
                if s in track:
                    ret.append(i)
            return ret

    def __getitem__(self, states):
        """
        Get the heuristic distance of a given Puzzle states dict.

        If maximumDepth is 0 and the state is not found then a KeyError is
        raised. If maximumDepth is nonzero and the state is not found, then
        the maximumDepth is returned.

        :param states:
        :return:
        """
        substate = self._flatten(states)
        for tDepth, tStates in enumerate(self.reverseTable):
            if substate in tStates:
                return tDepth
        else:
            if self.maximumDepth > 0:
                return self.maximumDepth
            else:
                raise KeyError(
                    "State %s is not in this PruningTable" % states)

    def get(self, states, default):
        """
        Get the heuristic distance of a given Puzzle states dict, or a default
        value if one is not found.

        The default would never be returned if maximumDepth was defined.

        :param states: dict. A Puzzle states dict.
        :param default: anything. A default value to return.
        :return:
        """
        try:
            return self[states]
        except KeyError:
            return default

    def __str__(self):
        ret = []
        for groupSpec in self.groupSpecs:
            row = [groupSpec['group'], groupSpec['subgroup']]

            if 'track' in groupSpec:
                row.append(groupSpec['track'])
                if 'distinctive' in groupSpec:
                    row.append(groupSpec['distinctive'])

            ret.append('[' + ' '.join(row) + ']')

        return 'Table: ' + ', '.join(ret)

    __repr__ = __str__