#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Puzzle simulation, utilities, and encapsulation

This module provides a means of representing a puzzle as a product of
mathematical groups and searching the resulting group for solutions.
"""

import heapq
from collections import deque
from copy import deepcopy
from datetime import datetime

from groupsolver.utils import PrioritySet
from groupsolver.group import Group
from groupsolver.heuristic import Heuristic

class Puzzle:
    """
    The Puzzle class represents the direct product of several groups.

    These groups are listed in the ``groups`` parameter. Elements of that list
    may be instances of :class:`Group <groupsolver.group.Group>` or dicts which
    describe Groups. Any dicts will be passed as kwargs to the Group
    constructor along with the ``accidentals`` given to this constructor.
    Passing dicts is the preferred way of constructing a puzzle. Moves will
    be applied to groups in the order that the groups are listed.

    The ``method`` parameter is used in the :meth:`solve` function. It is
    a list of :class:`Heuristic <groupsolver.heuristic.Heuristic>` objects or
    dicts which describe Heuristics. As with Groups, each dict will be passed
    to the Heuristic constructor along with this Puzzle. If the method is the
    string 'simple' then a single heuristic will be generated with the
    :meth:`groupsolver.heuristic.Heuristic` function.

    The Puzzle class is immutable. It can only manipulate states that are
    passed to it. For a mutable puzzle, see :class:`PuzzleInstance`. All
    public facing functions that manipulate states use the same format, which
    is a dict that maps group names to group elements. Group elements are
    dicts that map subgroups names to subgroup states (see
    :class:`groupsolver.group.Group`). For brevity, this will be referred to
    as a Puzzle states dict, or just "states" in parameter lists.

    Here is an example of a states dict::

        {
            'corners': {
                'orientations': [1, 0, 2, 2, 2, 2, 2, 2],
                'permutations': [0, 3, 2, 6, 4, 5, 1, 7]
            },
            'edges': {
                'permutations': [3, 0, 4, 5, 2, 1]
            }
        }

    :param name: str. The name of this puzzle. (Required)
    :param groups: list. A list of Groups or dicts describing dicts. (Required)
    :param accidentals: list. A list of accidental strings.
        (Optional, default None)
    :param method: list. A list of Heuristics. (Optional, 'simple' by default)
    """

    # Init functions

    def __init__(self, name=None, groups=None, accidentals=None,
                 method='simple', **kwargs):

        if name is None:
            raise ValueError('Puzzle must have a name')
        if groups is None:
            raise ValueError('Puzzle must have groups')

        self.name = name
        self.groups = dict()
        self.moveset = set()
        for groupName, group in groups.items():
            if isinstance(group, Group):
                self.groups[groupName] = group
            elif isinstance(group, dict):
                if accidentals and 'accidentals' not in group:
                    self.groups[groupName] = Group(accidentals=accidentals,
                                                   **group)
                else:
                    self.groups[groupName] = Group(**group)
            else:
                raise ValueError("Cannot coerce object with key %s to a Group"
                                 % groupName)
            self.moveset |= self.groups[groupName].moveset
        self.groupNames = sorted(self.groups.keys())

        # generate the blank state
        self._BLANK_STATE = dict()
        for groupName, group in self.groups.items():
            self._BLANK_STATE[groupName] = group.blankState()

        # must generate the heuristic at the end, since it uses Puzzle methods
        self.method = []
        if method == 'simple':
            self.method.append(Heuristic.simple(self))
        elif method is not None:
            if isinstance(method, Heuristic):
                self.method.append(method)
            elif isinstance(method, dict):
                self.method.append(Heuristic(self, **method))
            elif hasattr(method, '__iter__'):
                for heuristic in method:
                    if isinstance(heuristic, Heuristic):
                        self.method.append(heuristic)
                    elif isinstance(heuristic, dict):
                        self.method.append(Heuristic(self, **heuristic))
                    else:
                        raise ValueError("Expected all elements in method "
                                         "array to be or describe Heuristic "
                                         "objects. See: %s" % heuristic)
            else:
                raise ValueError("Expected method to be or describe a "
                                 "Heuristic object or a list of such things. "
                                 "See: %s" % method)

    # Private helper functions

    def _bfs(self, start, goal, moveset, **kwargs):
        """
        Perform a breadth first search from a given set of group states
        to another set of group states using only a given moveset.

        :param start: dict. The starting Puzzle states dict.
        :param goal: has a __contains__.
            Usually a list of Puzzle states dicts,
            or a :class:`groupsolver.heuristic.Heuristic`
            or a :class:`StateSet`
        :param moveset: list. A list of move strings than can be used.
        :return: list. A list of moves that transforms the start to the goal.
        """
        q = deque()
        q.append((start, tuple()))
        while q:
            state, path = q.popleft()
            if state in goal:
                return path

            for move in moveset:
                if path and path[-1][0] == move[0]:
                    continue

                nextState = dict()
                for groupName, group in self.groups.items():
                    nextState[groupName] = group.apply(move, state[groupName])
                q.append((nextState, path + (move,)))

        return None

    def _astar(self, start, goal, heuristic, moveset):
        """
        Perform an A* search with a given heuristic from a given set of group
        states to another set of group states using only a given moveset.

        :param start: dict. The starting Puzzle states dict.
        :param goal: has a __contains__.
            Usually a list of Puzzle states dicts,
            or a :class:`groupsolver.heuristic.Heuristic`
            or a :class:`StateSet`
        :param heuristic: :class:`Heuristic <groupsolver.heuristic.Heuristic>`.
            A heuristic to be used .
        :param moveset: list. A list of move strings than can be used.
        :return: list. A list of moves that transforms the start to the goal.
        """

        tStart = self._tupleState(start)
        closedSet = set()
        q = PrioritySet()
        q[tStart] = (heuristic(start), tuple())
        while q:
            tState, fScore, path = q.pop()
            closedSet.add(tState)
            state = self._untupleState(tState)
            if state in goal:
                return path

            for move in moveset:
                if path and path[-1][0] == move[0]:
                    continue

                neighbor = dict()
                for groupName, group in self.groups.items():
                    neighbor[groupName] = group.apply(move, state[groupName])
                tNeighbor = self._tupleState(neighbor)

                neighborFScore = len(path) + 1 + heuristic(neighbor)
                _, oldScore, _ = q.get(tNeighbor, (None, None))
                if ((oldScore is None or neighborFScore < oldScore) and
                        (tNeighbor not in closedSet)):
                    q[tNeighbor] = (neighborFScore, path + (move,))

        return None

    # assuming the goal contains the blankState for now
    def _idastar(self, start, goal, heuristic, moveset, initialBound=None,
                 verbose=False):
        """
        Perform an IDA* search with a given heuristic from a given set of group
        states to another set of group states using only a given moveset.
        Starts with a given initialBound if given.

        :param start: dict. The starting Puzzle states dict.
        :param goal: has a __contains__.
            Usually a list of Puzzle states dicts,
            or a :class:`groupsolver.heuristic.Heuristic`
            or a :class:`StateSet`
        :param heuristic: :class:`Heuristic <groupsolver.heuristic.Heuristic>`.
            A heuristic to be used.
        :param moveset: list. A list of move strings than can be used.
        :param initialBound: int. An initial upper bound on the recursive
            depth of the IDA* algorithm. (Optional, default calculated by
            heuristic)
        :return: list. A list of moves that transforms the start to the goal.
        """

        bound = initialBound or heuristic(start)
        prev = [start]
        path = []

        def search(g, bound):
            node = prev[-1]
            f = g + heuristic(node)
            if f > bound:
                return f
            if node in goal:
                return 'FOUND'

            #  Here, None acts as infinity.
            nearest = None

            # generate successors
            successors = []
            for move in moveset:
                if path and path[-1][0] == move[0]:
                    continue

                succ = dict()
                for groupName, group in self.groups.items():
                    succ[groupName] = group.apply(move, node[groupName])

                if succ not in prev:
                    # include the len(successors) here so heapq doesn't
                    # try to compare dicts
                    successors.append([heuristic(succ),
                                       len(successors),
                                       succ,
                                       move])

            # iterate in order of heuristic distance
            heapq.heapify(successors)
            while successors:
                _, _, succ, move = heapq.heappop(successors)

                prev.append(succ)
                path.append(move)
                exploration = search(g + 1, bound)
                if exploration == 'FOUND':
                    return 'FOUND'
                if nearest is None or exploration < nearest:
                    nearest = exploration
                path.pop()
                prev.pop()

            return nearest

        while True:
            I = datetime.now()
            if verbose:
                print("%s IDA* with bound %d" % (I, bound))
            t = search(0, bound)
            if t == 'FOUND':
                if verbose:
                    print("%s IDA* finished" % (datetime.now()))
                return path
            if t == None:
                return None
            bound = t

    def _tupleState(self, states):
        """
        Turns a state into a tuple of ints in a predictable order, namely
        in order of Group first, and putting orientations before permutations,
        concatenating all states.

        :param states: dict. A Puzzle states dict.
        :return:
        """
        ret = tuple()
        for groupName in self.groupNames:
            if self.groups[groupName].orientations:
                ret += tuple(states[groupName]['orientations'])
            if self.groups[groupName].permutations:
                ret += tuple(states[groupName]['permutations'])

        return ret

    def _untupleState(self, tupleState):
        """
        Converts a tuple created by :meth:`_tupleState` back to a Puzzle states
        dict.

        :param tupleState: tuple. A tuple created by ``_tupleState``.
        :return: dict. A Puzzle states dict.
        """
        ret = dict()
        i = 0
        for groupName in self.groupNames:
            delta = self.groups[groupName].size
            ret[groupName] = dict()
            if self.groups[groupName].orientations:
                ret[groupName]['orientations'] = list(tupleState[i:i+delta])
                i += delta
            if self.groups[groupName].permutations:
                ret[groupName]['permutations'] = list(tupleState[i:i+delta])
                i += delta

        return ret

    @staticmethod
    def _isSimpleState(states):
        """
        Given a dict, determines if it represents a Puzzle states dict as
        opposed to a StateSet description.

        :param states: dict. A dict that may or may not be a Puzzle states dict.
        :return: bool. True if ``states`` is a Puzzle states dict.
        """
        if not isinstance(states, dict):
            raise ValueError("Expected 'states' to be a dict")

        for groupName, groupState in states.items():
            if groupState == '*':
                return False

            if not isinstance(states, dict):
                raise ValueError("All group states must be of type 'dict'")

            for subgroupName, subgroupState in groupState.items():
                if subgroupState == '*':
                    return False

                if not hasattr(subgroupState, "__iter__"):
                    raise ValueError("Subgroup states must be iterable")

                if not hasattr(subgroupState, "__len__"):
                    raise ValueError("Subgroup states must define __len__")

                for groupElementCondition in subgroupState:
                    if not isinstance(groupElementCondition, int):
                        return False
        return True

    # Instance functions

    def blankState(self):
        """
        Returns a Puzzle states dict with all identity group elements, i.e.,
        a solved Puzzle.

        :return: dict. A solved Puzzle states dict.
        """

        return deepcopy(self._BLANK_STATE)

    def instance(self, states=None):
        """
        Returns a PuzzleInstance with a given initial state, or solved if not
        specified.

        :param states: dict or None. The initial state of the PuzzleInstance.
        :return: :class:`PuzzleInstance`. A PuzzleInstance with the given
            states.
        """
        if states:
            return PuzzleInstance(self, states)
        else:
            return PuzzleInstance(self, self.blankState())

    def apply_move(self, states, move):
        """
        Applies a single move to a given Puzzle states dict.

        :param states: dict. A Puzzle states dict.
        :param move: str. A move to be applied to the Puzzle states dict.
        :return: dict. The Puzzle states dict after applying the move.
        """
        ret = deepcopy(states)
        for groupName, states in ret.items():
            ret[groupName] = self.groups[groupName].apply(move, states)
        return ret

    def apply(self, states, movelist):
        """
        Apply multiple moves to a given Puzzle states dict.

        :param states: dict. A Puzzle states dict.
        :param movelist: list or str. Either a list of move strings or a single
            string containing space separated move strings.
        :return: dict. The Puzzle states dict after applying the move.
        """
        ret = deepcopy(states)
        if isinstance(movelist, str):
            for move in movelist.split():
                ret = self.apply_move(ret, move)
        elif hasattr(movelist, '__iter__'):
            for move in movelist:
                ret = self.apply_move(ret, move)
        else:
            raise ValueError("Expected movelist to be str or list of str. "
                             "See: %s" % movelist)

        return ret

    def solve(self, start, traversal='idastar', verbose=False):
        """
        Given a starting Puzzle states dict, produce a list of moves that
        brings the Puzzle to the blank state by applying the given graph
        traversal algorithm for each heuristic in this Puzzle's method.

        In other words, the chosen traversal algorithm will be applied
        with the first heuristic in self.methods, then the second, and
        so on, concatenating each result to the returned value.

        :param start: dict. A Puzzle states dict.
        :param traversal: str. A traversal string. Either
            * 'idastar' (default)
            * 'astar'
            * 'bfs'
        :param verbose: bool. Whether or not to print extra output. (Optional,
            default False)
        :return: list. A list of move strings which solves the Puzzle.
        """
        ret = []

        state = deepcopy(start)
        for heuristic in self.method:
            soln = self.search(start=state,
                               goal=heuristic,
                               traversal=traversal,
                               heuristic=heuristic,
                               moveset=heuristic.moveset,
                               verbose=verbose)
            if soln:
                state = self.apply(state, soln)
                ret.extend(soln)
            elif soln is None:
                return None

        return ret

    def search(self, start=None, goal=None, traversal=None, heuristic=None,
               moveset=None, **kwargs):
        """
        Apply a given traversal algorithm with a given heuristic to a given
        start state in order to find a sequence of moves in the given moveset
        that brings the start state into a state that satisfies the goal.

        Valid goals include:

            * None (meaning the blankState. This is the default)
            * Another Puzzle states dict
            * A :class:`StateSet`
            * A dict describing a StateSet
            * A Heuristic

        :param start: dict. A Puzzle states dict.
        :param goal: dict, StateSet, Heuristic, or None. See above. (Optional,
            default None)
        :param traversal: str. A traversal string. Either
            * 'idastar' (default)
            * 'astar'
            * 'bfs'
        :param heuristic: :class:`Heuristic <groupsolver.heuristic.Heuristic>`.
            A heuristic to be used.
        :param moveset: list. A list of move strings. (Optional, default None
            will take this moveset from the Puzzle.moveset)
        :param verbose: bool. Whether or not to print extra output. (Optional,
            default False)
        :return:
        """

        if moveset is None:
            moveset = self.moveset

        # start state is blank state by default
        if start is None:
            start = self.blankState()

        # goal state is blank state by default
        if goal is None:
            goal = [self.blankState()]

        # heuristic goals speak for themselves
        elif isinstance(goal, Heuristic):
            pass

        # everything but bfs needs a start state conversion
        elif traversal != 'bfs':

            # simple goal needs left composition
            if Puzzle._isSimpleState(goal):
                newStart = dict()
                for groupName, state in goal.items():
                    newStart[groupName] = self.groups[groupName]\
                        .composeLeftInverse(goal[groupName], start[groupName])
                start, goal = newStart, [self.blankState()]

            # StateSet goal needs validation
            else:
                goal = StateSet(self, goal)

        # by convention, the goal should always has a __contains__
        else:
            goal = [goal]

        if heuristic is None and len(self.method) == 1:
            heuristic = self.method[0]

        if traversal is None:
            traversal = 'idastar' if heuristic else 'bfs'

        if traversal == 'idastar':
            if heuristic:
                return self._idastar(start, goal, heuristic, moveset, **kwargs)
            else:
                raise ValueError("Cannot run IDA* without a heuristic")
        if traversal == 'astar':
            if heuristic:
                return self._astar(start, goal, heuristic, moveset, **kwargs)
            else:
                raise ValueError("Cannot run IDA* without a heuristic")
        if traversal == 'bfs':
            return self._bfs(start, goal, moveset, **kwargs)

    # Override functions

    def __str__(self):
        ret = ['%s (Puzzle)' % self.name]
        for groupName, group in self.groups.items():
            ret.append('  %s: %s' % (groupName, group))
        return '\n'.join(ret)

    __repr__ = __str__

class StateSet:
    """
    Represents a set of possible end goals for a Puzzle's search.

    The format of the description of a StateSet is almost idential to the
    format of a Puzzle states dict, except that any int can be replaced by
    anything with a __contains__ method, and any int, subgroup state, or group
    element may be replaced with the string '*' to indicate that it can have
    any value.

    For example, below is a dict describing a StateSet in which the corners
    orientations must be all 0, the corners permutations are irrelevant, and
    the 0th edge can be 1 or 2, the 1st edge can be 0 or 2, the 4th edge can
    be 3 or 5, and the rest of the edges can be anything.::

        {
            'corners': {
                'orientations': [0, 0, 0, 0, 0, 0, 0, 0],
                'permutations': '*',
            },
            'edges': {
                'permutations': [[1, 2], [0, 2], '*', '*', [3, 5], '*']
            }
        }

    :param puzzle: :class:`Puzzle`. The Puzzle for this StateSet.
    :param description: dict. As explained above.
    :param strict: bool. Whether or not to raise an exception if a state
        passed to the __contains__ function does not fit the format of a
        Puzzle states dict for the given Puzzle. (Optional, default True)
    """
    def __init__(self, puzzle, description, strict=True):

        self.strict = strict

        # validate description
        if not isinstance(description, dict):
            raise ValueError("Expected description to be a dict")

        self.description = dict()
        for groupName, groupState in description.items():
            if groupName not in puzzle.groups:
                raise ValueError("No group named %s in Puzzle named %s"
                                 % (groupName, puzzle.name))

            if groupState == '*':
                self.description[groupName] = '*'
                continue

            if not isinstance(groupState, dict):
                raise ValueError(
                    "All group states must be '*' or be of type 'dict'")

            self.description[groupName] = dict()
            for subgroupName, subgroupState in groupState.items():
                if subgroupState == '*':
                    self.description[groupName][subgroupName] = '*'
                    continue

                if not hasattr(subgroupState, "__iter__"):
                    raise ValueError("Subgroup states must be iterable")

                if not hasattr(subgroupState, "__len__"):
                    raise ValueError("Subgroup states must define __len__")

                if len(subgroupState) != puzzle.groups[groupName].size:
                    raise ValueError("Subgroup states must be the same length "
                                     "as the group size. See subgroup %d of "
                                     "group %s in puzzle %s "
                                     % (subgroupName, groupName, puzzle.name))

                self.description[groupName][subgroupName] = subgroupState
                for i, groupElementCondition in enumerate(subgroupState):
                    if groupElementCondition == '*':
                        pass
                    elif isinstance(groupElementCondition, int):
                        pass
                    elif hasattr(groupElementCondition, '__contains__'):
                        if hasattr(groupElementCondition, '__iter__'):
                            self.description[groupName][subgroupName][i] = \
                                set(groupElementCondition)
                    else:
                        raise ValueError("Group Element Condition %s is "
                                         "neither an int nor does it have "
                                         "a __contains__ method."
                                         % groupElementCondition)

    def __contains__(self, states):
        """
        True if the given state satisfies the description of this StateSet.
        If ``state`` is not a valid Puzzle states dict, then False is returned
        unless this StateSet was created with strict=True. Then a
        ``ValueError`` is raised.

        :param states: dict. A Puzzle states dict.
        :return: True if this state satisfies the StateSet description.
        """
        for groupName, groupCondition in self.description.items():
            if groupCondition == '*':
                continue

            if groupName not in states:
                if self.strict:
                    raise ValueError("StateSet has groupName %s that is not "
                                     "in given state %s" % (groupName, states))
                else:
                    return False

            groupState = states[groupName]
            if len(groupState) != len(groupCondition):
                if self.strict:
                    raise ValueError(
                        "StateSet's group named %s is length %s, whereas "
                        "the given state's group is length %s"
                         % (groupName, len(groupCondition), len(groupState)))
                else:
                    return False

            for subgroupName, subgroupCondition \
                    in self.description[groupName].items():
                if subgroupCondition == '*':
                    continue

                subgroupState = groupState[subgroupName]
                if len(subgroupState) != len(subgroupCondition):
                    if self.strict:
                        raise ValueError(
                            ("StateSet's subgroup named %s is length " +
                             "%s, whereas the given state's subgroup " +
                             "is length %s") %
                            (subgroupName, len(subgroupCondition),
                             len(subgroupState)))
                    else:
                        return False

                for stateElement, stateElementCondition \
                        in zip(subgroupState, subgroupCondition):
                    if stateElementCondition == '*':
                        continue

                    if isinstance(stateElementCondition, int):
                        if stateElement != stateElementCondition:
                            return False
                    else:  # it must be a set, if not an int or '*'
                        if stateElement not in stateElementCondition:
                            return False

        return True

class PuzzleInstance:
    """
    PuzzleInstance implements a mutable puzzle which tracks its state as moves
    are applied to it

    It's best to use :meth:`Puzzle.instance` to get a PuzzleInstance.

    :param puzzle: :class:`Puzzle`
    :param states: A Puzzle states dict. (Optional, default None means the
        Puzzle states dict returned by :meth:`Puzzle.blankState`)
    """

    def __init__(self, puzzle, states=None, **kwargs):
        self.puzzle = puzzle
        # TODO assert that states are valid
        if states is None:
            self.states = self.puzzle.blankState()
        else:
            self.states = states

    def reset(self):
        """
        Resets this puzzle to the blankState.
        """
        self.states = self.puzzle.blankState()

    def apply(self, movelist):
        """
        Apply multiple moves to this PuzzleInstance.

        :param movelist: list or str. Either a list of move strings or a single
            string containing space separated move strings.
        """

        if isinstance(movelist, str):
            for move in movelist.split():
                self._apply_move(move)
        elif hasattr(movelist, '__iter__'):
            for move in movelist:
                self._apply_move(move)
        else:
            raise ValueError("Expected movelist to be str or list of str. "
                             "See: %s" % movelist)

    def _apply_move(self, move):
        """
        Applies a single move to this PuzzleInstance.

        :param move: str. A move to be applied to the Puzzle states dict.
        """

        for groupName, state in self.states.items():
            self.states[groupName] = \
                self.puzzle.groups[groupName].apply(move, state)

    def solve(self, traversal='idastar', verbose=False):
        """
        Passes the current state to the parent Puzzle's
        :meth:`solve() <groupsolver.puzzle.Puzzle.solve>` function with
        the given traversal and verbosity

        :param traversal: str. A traversal string. Either
            * 'idastar' (default)
            * 'astar'
            * 'bfs'
        :param verbose: bool. Whether or not to print extra output. (Optional,
            default False)
        :return: list. A list of move strings which solves the Puzzle.
        """
        return self.puzzle.solve(start=self.states,
                                 traversal=traversal,
                                 verbose=verbose)

    def __str__(self):
        ret = ['%s (PuzzleInstance)' % self.puzzle.name]
        for groupName, state in self.states.items():
            ret.append('  %s: %s' % (groupName, state))
        return '\n'.join(ret)

    __repr__ = __str__