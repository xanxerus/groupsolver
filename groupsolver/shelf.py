#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A small library of Puzzles to use out of the box
"""

from groupsolver.puzzle import Puzzle

def Cuboid3x3x2():
    """
    A simple 3x3x2 Cuboid. The upper face has 9 stickers. The R, L, F, and B
    turns are all 180 degrees. A good example of some moves needing accidentals
    while others do not, since U and D have "'" and "2" accidentals but
    R, L, F, and B do not.
    """

    return Puzzle(**{
        'name': 'Cuboid 3x3x2',
        'accidentals': {
            'U': ["'", "2"],
            'D': ["'", "2"],
        },
        'groups': {
            'edges': {
                'size': 8,
                'permutations': {
                    'U': [0, 1, 2, 3],
                    'D': [4, 7, 6, 5],
                    'F': [0, 4],
                    'R': [1, 5],
                    'B': [2, 6],
                    'L': [3, 7],
                },
            },
            'corners': {
                'size': 8,
                'permutations': {
                    'U': [0, 1, 2, 3],
                    'D': [4, 7, 6, 5],
                    'F': [[0, 5], [1, 4]],
                    'R': [[1, 6], [2, 5]],
                    'B': [[2, 7], [3, 6]],
                    'L': [[0, 7], [3, 4]],
                },
            },
        },
    })

def Cubominx():
    """
    A very simple puzzle whose corners only orient and whose edges only
    permute. It was first physically made by Tony Fisher as the Cubominx.
    This has also been called a Ivy Leaf Cube after being mass produced by
    the MoYu brand.

    Its entire state table can fit into RAM. Its group radius is 8. It's a
    personal favorite.
    """

    return Puzzle(**{
        'name': 'Cubominx',
        'accidentals': ["'"],
        'groups': {
            'edges': {
                'size': 6,
                'permutations': {
                    'F': [0, 2, 1],
                    'U': [1, 5, 4],
                    'R': [2, 3, 5],
                    'L': [0, 4, 3],
                },
            },
            'corners': {
                'size': 4,
                'modulus': 3,
                'orientations': {
                    'F': {0: -1},
                    'U': {1: -1},
                    'R': {2: -1},
                    'L': {3: -1},
                },
            },
        },
    })

def GearCubeExtreme():
    """
    A puzzle invented by Oskar Van Deventer. Every one of its 8 gears can be
    in one of 6 orientations with no parity invariants to reduce the group
    size. This is a good puzzle to use for testing partial goals, since
    solving the whole thing is very hard, at least in version 1.0.0 of this
    library.
    """

    side_accidentals = ["'"]
    for i in range(2, 6):
        side_accidentals.append(str(i))
        side_accidentals.append(str(i) + "'")

    return Puzzle(**{
        'name': 'GearCubeExtreme',
        'accidentals': {
            'U': ["'", "2"],
            'D': ["'", "2"],
            'R': side_accidentals,
            'B': side_accidentals,
        },
        'groups': {
            'centers': {
                'size': 6,
                'permutations': {
                    'R': [0, 3, 5, 1],
                    'B': [1, 2, 3, 4],
                }
            },
            'edges': {
                'size': 4,
                'permutations': {
                    'R': [1, 2],
                    'B': [2, 3],
                },
            },
            'corners': {
                'size': 8,
                'permutations': {
                    'U': [0, 1, 2, 3],
                    'D': [4, 7, 6, 5],
                    'R': [[1, 6], [2, 5]],
                    'B': [[2, 7], [3, 6]],
                },
            },
            'gears': {
                'size': 8,
                'modulus': 6,
                'permutations': {
                    'U': [0, 1, 2, 3],
                    'D': [4, 7, 6, 5],
                    'R': [[1, 5], [0, 4, 6, 2]],
                    'B': [[2, 6], [1, 5, 7, 3]],
                },
                'orientations': {
                    'R': {0: 1, 2: 1, 4: 1, 6: 1},
                    'B': {1: 1, 3: 1, 5: 1, 7: 1},
                }
            },
        },
        'method': {
            'tables': [
                {'groups': [{'group': 'centers'}, {'group': 'edges'}]},
                {'groups': [{'group': 'corners'}]},
                {'groups': [{'group': 'gears', 'subgroup': 'permutations'}]},
                {'groups': [{'group': 'gears', 'subgroup': 'orientations'}],
                 'maximumDepth': 8}
            ]
        }
    })

def RubiksCube():
    """
    The classic Rubik's Cube with Kociemba's two-phase algorithm implemented
    with small pruning tables. Phase 1 goes quickly, but phase 2 does not.
    Many improvements will need to be made to solve a puzzle like this.
    """
    g2moves = ['F2', 'R2', 'L2', 'B2', 'U', 'U2', "U'", 'D', 'D2', "D'"]

    return Puzzle(**{
        'name': "Rubik's Cube",
        'accidentals': ["'", "2"],
        'groups': {
            'edges': {
                'size': 12,
                'modulus': 2,
                'permutations': {
                    'F': [0, 7, 8, 4],
                    'U': [0, 1, 2, 3],
                    'R': [1, 4, 9, 5],
                    'D': [8, 11, 10, 9],
                    'L': [3, 6, 11, 7],
                    'B': [2, 5, 10, 6]
                },
                'orientations': {
                    'F': {0: 1, 7: 1, 8: 1, 4: 1},
                    'B': {2: 1, 5: 1, 10: 1, 6: 1},
                }
            },
            'corners': {
                'size': 8,
                'modulus': 3,
                'permutations': {
                    'F': [0, 4, 5, 1],
                    'U': [0, 1, 2, 3],
                    'R': [1, 5, 6, 2],
                    'D': [4, 7, 6, 5],
                    'L': [0, 3, 7, 4],
                    'B': [3, 2, 6, 7]
                },
                'orientations': {
                    'F': {0: 2, 1: 1, 5: 2, 4: 1},
                    'R': {1: 2, 2: 1, 6: 2, 5: 1},
                    'B': {2: 2, 6: 1, 7: 2, 3: 1},
                    'L': {3: 2, 7: 1, 4: 2, 0: 1},
                }
            }
        },
        'method': [{
            'tables': [
                {'maximumDepth': 6, 'groups': [
                    {
                        'group': 'edges',
                        'subgroup': 'orientations'
                    }, {
                        'group': 'edges',
                        'subgroup': 'permutations',
                        'track': [4, 5, 6, 7],
                        'distinctive': False
                    },
                ], },
                {'groups': [
                    {
                        'group': 'corners',
                        'subgroup': 'orientations'
                    }
                ]}
            ]
        }, {
            'moveset': g2moves,
            'tables': [{
                'maximumDepth': 6,
                'groups': [
                    {
                        'group': 'edges',
                        'subgroup': 'permutations',
                    }
                ],
            }, {
                'maximumDepth': 0,
                'groups': [
                    {
                        'group': 'corners',
                        'subgroup': 'permutations'
                    }
                ],
            }]
        }]
    })

def Skewb():
    """
    The fan favorite Skewb, well known as a Meffert's puzzle, used as the base
    for many shape mods including the Skewb Ultimate, the Golden Cube, etc.
    Even the Cubominx is just a Skewb with four fewer corners.
    """
    return Puzzle(**{
        'name': 'Skewb',
        'accidentals': ["'"],
        'groups': {
            'edges': {
                'size': 6,
                'permutations': {
                    'F': [0, 2, 1],
                    'U': [1, 5, 4],
                    'R': [2, 3, 5],
                    'L': [0, 4, 3],
                },
            },
            'corners': {
                'size': 8,
                'modulus': 3,
                'orientations': {
                    'F': {0: -1, 3: 1, 1: 1, 4: 1},
                    'U': {2: -1, 1: 1, 3: 1, 6: 1},
                    'R': {5: -1, 4: 1, 1: 1, 6: 1},
                    'L': {7: -1, 3: 1, 4: 1, 6: 1},
                },
                'permutations': {
                    'F': [1, 3, 4],
                    'U': [1, 6, 3],
                    'R': [1, 4, 6],
                    'L': [3, 6, 4],
                }
            },
        },
    })

def SkewbUltimate():
    """
    This dodecahedral shape mod of the Skewb has the same group structure
    as a normal Skewb, if the normal Skewb's edges had orientations. It
    makes it much more difficult to deal with computationally.
    """
    return Puzzle(**{
        'name': 'SkewbUltimate',
        'accidentals': ["'"],
        'groups': {
            'edges': {
                'size': 6,
                'modulus': 4,
                'orientations': {
                    'F': {0: 3, 1: 2, 2: 3},
                    'U': {1: 0, 4: 3, 5: 1},
                    'R': {2: 3, 3: 0, 5: 1},
                    'L': {0: 3, 3: 2, 4: 3},
                },
                'permutations': {
                    'F': [0, 2, 1],
                    'U': [1, 5, 4],
                    'R': [2, 3, 5],
                    'L': [0, 4, 3],
                },
            },
            'corners': {
                'size': 8,
                'modulus': 3,
                'orientations': {
                    'F': {0: -1, 3: 1, 1: 1, 4: 1},
                    'U': {2: -1, 1: 1, 3: 1, 6: 1},
                    'R': {5: -1, 4: 1, 1: 1, 6: 1},
                    'L': {7: -1, 3: 1, 4: 1, 6: 1},
                },
                'permutations': {
                    'F': [1, 3, 4],
                    'U': [1, 6, 3],
                    'R': [1, 4, 6],
                    'L': [3, 6, 4],
                }
            },
        },
    })